<?php

namespace App\WebServices;
namespace App\Http\Controllers;


use App\WebServices\WSDLDocument;
use App\WebServices\ZoologicoWebService;
use Illuminate\Http\Request;
use SoapServer;

class SoapServerController extends Controller
{
    private $clase= ZoologicoWebService::class;
    private $uri ='http://zoologico.laravel/api';
    private $urlWSDL='http://zoologico.laravel/api/wsdl';

    public function getServer(){
        //
    $server=new SoapServer($this->urlWSDL);
    $server->setClass($this->clase);
    $server->handle();
    exit;
    }

    public function getWSDL(){
        //
        $wsdl=new WSDLDocument($this->clase, $this->uri, $this->uri);
        $wsdl->formatOutput=true;
        header('Content-Type: text/xml');
        echo $wsdl->saveXML();
        exit;

    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
