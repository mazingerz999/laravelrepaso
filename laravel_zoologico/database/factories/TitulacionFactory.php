<?php

namespace Database\Factories;

use App\Models\titulacion;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TitulacionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = titulacion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
         $nombre=$this->faker->catchPhrase;
        return [


            'nombre'=>$nombre,
             'slug'=>Str::slug($nombre),

        ];
    }
}
