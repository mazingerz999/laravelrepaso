@extends('layouts.master')

@section('titulo')
    Zoologico
@endsection

@section('contenido')

<div class="row">
    @foreach( $animales as $clave => $animal )
    <div class="col-xs-12 col-sm-6 col-md-4 ">

    <img src="{{asset('assets/imagenes')}}/{{$animal->imagen}}" style="height:200px"/>
    <h4 style="min-height:45px;margin:5px 0 10px 0">
        <a href="{{ route('animales.show' , $animal ) }}">{{$animal->especie}}</a>
    </h4>
    </a>
    Revisiones: {{count($animal->revisiones)}}
    </div>
    <ul>
    @foreach( $animal->cuidadores as $cuidador )
    <li>{{$cuidador->nombre}}</li>
    <a href="{{ route('cuidadores.show' , $cuidador ) }}">Ver Cuidador</a>
    @endforeach
    </ul>

    @endforeach
</div>
@endsection
